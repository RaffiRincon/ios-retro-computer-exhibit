//
//  Retro_Computer_Exhibit_App_ClipApp.swift
//  Retro Computer Exhibit App Clip
//
//  Created by Rafael Rincon on 8/1/20.
//

import SwiftUI

@main
struct Retro_Computer_Exhibit_App_ClipApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
