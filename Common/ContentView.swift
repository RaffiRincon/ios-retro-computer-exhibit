//
//  ContentView.swift
//  Retro Computer Exhibit
//
//  Created by Rafael Rincon on 8/2/20.
//

import SwiftUI
import Firebase

struct ContentView: View {
    @State var command = ""
    
    var body: some View {
        TextField("Command", text: $command) {
            Firestore.firestore().collection("Machines").document("Commodore 128").setData(["latestCommand": command], merge: true)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
